/**
 * Document: MaxCompiler Tutorial (maxcompiler-tutorial.pdf)
 * Chapter: 6      Example: 3      Name: Vectors
 * MaxFile name: Vectors
 * Summary:
 *    Streams a vector of integers to the dataflow engine and confirms that the
 *    returned stream contains the integers values doubled.
 */

#include <stdio.h>
#include <stdlib.h>
#include "Maxfiles.h"
#include <MaxSLiCInterface.h>

int main()
{
	const int vectorSize = Vectors_vectorSize;
	const int streamSize = 1;
	size_t sizeBytes = vectorSize * vectorSize * sizeof(uint32_t);
	uint32_t *inVector1 = malloc(sizeBytes);
	uint32_t *inVector2 = malloc(sizeBytes);
	uint32_t *outVector = malloc(sizeBytes);

	for (int i = 0; i < vectorSize; i++) {
	    for (int j = 0; j < vectorSize; j++) {
	        inVector1[i*vectorSize + j] = i + j + 1;
	        inVector2[i*vectorSize + j] = i*j + 100;
	    }
	}
	
	for (int i = 0; i < vectorSize; i++) {
	    for (int j = 0; j < vectorSize; j++) {
	        printf("%d ", inVector1[i*vectorSize + j]);
	    }
	    printf("\n");
	}
	
	for (int i = 0; i < vectorSize; i++) {
	    for (int j = 0; j < vectorSize; j++) {
	        printf("%d ", inVector2[i*vectorSize + j]);
	    }
	    printf("\n");
	}

	for (int i = 0; i < vectorSize * vectorSize; i++) {
		outVector[i] = 0;
	}

	printf("Running DFE.\n");
	Vectors(streamSize, inVector1, inVector2, outVector);
	
	for (int i = 0; i < vectorSize; i++) {
	    for (int j = 0; j < vectorSize; j++) {
	        printf("%d ", outVector[i*vectorSize + j]);
	    }
	    printf("\n");
	}

	return 0;
}
